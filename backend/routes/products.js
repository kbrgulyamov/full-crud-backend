const express = require("express")
const Products = require("../models/products")
const Users = require("../models/users")
let Post = require("../models/products")
const { findById } = require("../models/users")
const router = express.Router()

// Так пишем сейчас
router.get("/", async (req, res) => {
    try {
        let body = await Products
            .find()
            .populate("authorId")


        res.json({
            title: "goods",
            description: "Here is all goods",
            isDone: true,
            body
        })
    } catch (error) {
        console.log(error)

        res.json({
            ok: false,
            messege: "Seems there no goods!",
            error
        })
    }
})

router.get("/:id", async (req, res) => {
    try {
        let good = await Products
            .findById(req.params.id)
            .populate("authorId")

        Products.findOneAndUpdate({ _id: req.params.id }, { $inc: { counter: 1 } }, (err, res) => {
            if (err) {
                console.log(err);
            } else console.log(res);
        })

        res.json({
            ok: true,
            message: "Element found",
            element: good
        })
    } catch (error) {

    }
})

router.post("/", async (req, res) => {
    try {
        Products.create(req.body, async (error, data) => {

            if (error) {
                console.log(error);
                res.json({
                    ok: false,
                    message: "Error inside callback",
                    error
                })
            } else {
                let user = await Users.findById(data.authorId)
                user.products.push(data.id)
                user.productsCount = user.products.length
                user.save()

                res.json({
                    ok: true,
                    message: "Element created!",
                    data: req.body
                })
            }
        })
    } catch (error) {
        console.log(error);
        res.json({
            ok: false,
            message: "Some error",
            error
        })
    }
})

router.patch("/:id", async (req, res) => {
    try {
        Products.findByIdAndUpdate(req.params.id, req.body, (error, data) => {
            if (error) {
                res.json({
                    ok: false,
                    messege: "Seems there no goods!",
                    error
                })
            } else {
                res.json({
                    ok: true,
                    messege: "Update!",
                    element: req.body,
                })
            }
        })
    } catch (error) {
        res.json({
            ok: false,
            messege: "Seems there no goods!",
            error
        })
    }
    console.log("patch elem");
})

router.delete("/:id", async (req, res) => {
    console.log(req.params.id);
    console.log(req.body.id);
    Products.findByIdAndDelete(req.params.id, async (error, data) => {
        if (error) {
            res.json({
                ok: false,
                messege: "Deleted shit!",
                el: data,
                error
            })
        } else {
            res.json({
                ok: true,
                message: 'Deleted',
                element: data,
            })
        }
    })
})

// router.patch("/:id", async (req , res) =>) {
//     Post.findOneAndUpdate({ _id: res._id }, { $inc: { views: 1 } }, {new: true }, (err, response) => {
//             if (err) {
//             callback(err);
//            } else {
//             callback(response);
//            }
// }
// Models.post.Post.findOneAndUpdate({ _id: res._id }, { $inc: { views: 1 } }, {new: true }, (err, response) => {
//     if (err) {
//     callback(err);
//    } else {
//     callback(response);
//    }

module.exports = router