const mongoose = require('mongoose')


const Users = mongoose.Schema({
    name: {
        type: String,
        required: false,
        default: "Adam Adams"
    },
    photo: {
        type: String,
        default: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR5jz3EjzrTucUV0OMhBzD1GJGk4eBRDwfpUe5suv9Dog&s"
    },
    password: {
        type: Number,
        required: true
    },
    productsCount: {
        type: Number,
        default: 0
    },
    products: {
        type: mongoose.Schema.Types.Array,
        ref: "Products"
    }

})


module.exports = mongoose.model('Users', Users)
