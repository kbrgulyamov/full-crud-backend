// В этом файле пропиписываем все модели данных
module.exports = {
    // берем файлы в этой же папке
    products: require("./products.js"),
    users: require("./users.js")
}