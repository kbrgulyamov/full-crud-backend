const mongoose = require('mongoose')

// Называние схем всегда во множественном числе
// Ключи вашей схемы крайне опасно менять/удалять
const Products = mongoose.Schema({
    authorId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Users",
        required: true
    },
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        default: "One of the product"
    },
    price: {
        type: Number,
        required: true
    },
    counter: {
        type: Number,
        default: 0
    }
})


module.exports = mongoose.model('Products', Products)

